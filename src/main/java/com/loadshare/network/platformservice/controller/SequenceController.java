package com.loadshare.network.platformservice.controller;

import com.loadshare.network.platformservice.dao.SequenceRepository;
import com.loadshare.network.platformservice.dao.entity.Sequence;
import com.loadshare.network.platformservice.dao.impl.SequenceRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="/sequence")
@SpringBootApplication(scanBasePackages = "com.loadshare.network.platformservice")
@EntityScan( basePackages = {"com.loadshare.network.platformservice.dao"} )
@EnableJpaRepositories("com.loadshare.network.platformservice.dao")
public class SequenceController {

    @Autowired
    private SequenceRepositoryImpl sequenceRepository;

    @GetMapping(path = "/generate")
    public @ResponseBody
    Sequence generate(@RequestParam("partition") String partition,
                          @RequestParam("referenceType") String referenceType,
                          @RequestParam("referenceId") String referenceId) {
        return sequenceRepository.generate(partition, referenceType, referenceId);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SequenceController.class, args);
    }
}
