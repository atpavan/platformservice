package com.loadshare.network.platformservice.dao.impl;

import com.loadshare.network.platformservice.dao.SequenceRepository;
import com.loadshare.network.platformservice.dao.entity.Sequence;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
@Transactional
public class SequenceRepositoryImpl implements SequenceRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Sequence generate(String partition, String referenceType, String referenceId) {
        Query query = entityManager.createNativeQuery
                ("INSERT INTO `sequence` (`partition`, `reference_type`, `reference_id`) " +
                "VALUES (:partition, :referenceType, :referenceId) ON DUPLICATE KEY " +
                "UPDATE last_modified_on = now(), number = number + 1, version = version + 1");
        query.setParameter("partition", partition);
        query.setParameter("referenceType", referenceType);
        query.setParameter("referenceId", referenceId);
        query.executeUpdate();

        // TODO: Need to make this entire method atomic

        TypedQuery<Sequence> resultQuery = entityManager.createNamedQuery("fetchSequence",Sequence.class);
        resultQuery.setParameter("partition", partition);
        resultQuery.setParameter("referenceType", referenceType);
        resultQuery.setParameter("referenceId", referenceId);

        return resultQuery.getSingleResult();
    }
}
