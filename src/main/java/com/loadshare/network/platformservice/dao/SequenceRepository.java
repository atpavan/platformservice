package com.loadshare.network.platformservice.dao;

import com.loadshare.network.platformservice.dao.entity.Sequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@NoRepositoryBean
public interface SequenceRepository{

    /**
     * Generates a PIN for given set of conditions
     * Increments or creates new row based on the conditions
     *
     * @param partition - Usually a date (format: YYYY-mm-dd
     * @param referenceType -- Usually "USER"
     * @param referenceId -- Id of reference type
     * @return
     */
    @Transactional
    public Sequence generate(@Param("partition") String partition,
                             @Param("referenceType") String referenceType,
                             @Param("referenceId") String referenceId);

}
