package com.loadshare.network.platformservice.dao.entity;

import com.loadshare.network.platformservice.dao.base.BaseEntity;

import javax.persistence.*;


@NamedQueries(
        @NamedQuery(name = "fetchSequence", query = "select s from Sequence s where " +
                "partition = :partition and referenceType = :referenceType and referenceId = :referenceId")
)
@Entity
@DiscriminatorValue("sequence")
public class Sequence extends BaseEntity {

    private String partition;

    private String referenceType;

    private String referenceId;

    private Long number;

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "partition='" + partition + '\'' +
                ", referenceType='" + referenceType + '\'' +
                ", referenceId='" +  + '\'' +
                ", number=" + number +
                '}';
    }
}
