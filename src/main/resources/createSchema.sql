CREATE TABLE `sequence` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT 'SYSTEM',
  `last_modified_by` varchar(50) NOT NULL DEFAULT 'SYSTEM',
  `partition` varchar(50) NOT NULL DEFAULT 'DEFAULT',
  `reference_type` varchar(50) NOT NULL DEFAULT 'DEFAULT',
  `reference_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE (`partition`, `reference_type`, `reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4

--
-- Dumping data for table `truck`
--

INSERT INTO `truck` VALUES (1,100,'KA01 AB 0101');
